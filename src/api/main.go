package main

import (
	"log"
	"io/ioutil"
	"net/http"
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	router.GET("/coupons", getCoupons)

	router.Run(":8080")
}

func getCoupons(c *gin.Context) {
	params := c.Request.URL.Query()
	query_string := ""

	if _, ok := params["limit"]; ok {
		query_string += "&limit=" + params["limit"][0]
	}

	if _, ok := params["brand"]; ok {
		query_string += "&brand=" + params["brand"][0]
	}

	if _, ok := params["value"]; ok {
		query_string += "&value=" + params["value"][0]
	}

	client := &http.Client{}
	req, err := http.NewRequest("GET", "http://localhost:8000/get-coupons?"+query_string[1:], nil)
	if err != nil {
		log.Fatal(err)
	}
	
	req.Header.Add("Authorization", "Bearer JghLN5AblzzH44q7fWbO90o31E7egR3E")
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	
	c.JSON(http.StatusOK, string(body))
	return
}